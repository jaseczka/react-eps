/** @jsx React.DOM */
var React = require('react/addons');
var _ = require('lodash');
var CalendarHeatMap = require('reacteps').CalendarHeatMap;
var FilterPanel = require('reacteps').FilterPanel;
var FilterPanelMixin = require('reacteps').FilterPanelMixin;
var Modal = require('react-bootstrap').Modal;
var OverlayMixin = require('react-bootstrap').OverlayMixin;
var testdata = require('./testdata');
var moment = require('moment');

var width = 500;
var height = 500;

var FilteredCalendar = React.createClass({
    mixins: [FilterPanelMixin],

    getInitialState: function() {
        return {
            filters: testdata.getFilters()
        }
    },

    render: function() {
        var rawData = testdata.getRawData();
        /* getFilteredData from FilterPanelMixin */
        var filteredData = this.getFilteredData(rawData);
        var aggregatedData = testdata.aggregateData(filteredData);
        var calendarData = aggregatedData.data;
        var modalData = aggregatedData.tooltips;
        return (
            <div>
                {this.renderFilterPanel()}
                <Calendar
                    calendarData={calendarData}
                    modalData={modalData}
                    initialDateFrom={moment().year(2015).month(0).date(1)}
                />
            </div>
        )
    }
});

var Calendar = React.createClass({
    //bootstrap mixin that allows for custom modal state management
    mixins: [OverlayMixin],

    getDefaultProps: function() {
        return {
            initialDateFrom: moment(),
            initialDateTo: moment().add(28, 'd')
        }
    },

    getInitialState: function() {
        return {
            isModalVisible: false,
            dateFrom: this.props.initialDateFrom,
            dateTo: this.props.initialDateTo
        };
    },

    openModal: function(date) {
        this.setState({
            isModalOpen: true,
            modalDate: date
        });
    },

    closeModal: function() {
        this.setState({
            isModalOpen: false
        });
    },

    changeDateFrom: function(moment) {
        this.setState({dateFrom: moment});
    },

    changeDateTo: function(moment) {
        this.setState({dateTo: moment});
    },

    render: function() {
        return (
            <div>
                <div>
                    <label>from
                        <DatePicker
                            selected={this.state.dateFrom}
                            onChange={this.changeDateFrom}
                            dateFormat="MM/DD/YYYY"
                            maxDate={this.state.dateTo}
                        />
                    </label>

                    <label>to
                        <DatePicker
                            selected={this.state.dateTo}
                            onChange={this.changeDateTo}
                            dateFormat="MM/DD/YYYY"
                            minDate={this.state.dateFrom}
                        />
                    </label>
                </div>

                <CalendarHeatMap
                    handleClick={this.openModal}
                    data={this.props.calendarData}
                    mode="daily"
                    dateFrom={this.state.dateFrom.toDate()}
                    dateTo={this.state.dateTo.toDate()}
                />
            </div>
        );
    },

    //OverlayMixin render function uses this:
    renderOverlay: function() {
        if (!this.state.isModalOpen) {
            return <span />;
        }
        var tooltip = _.find(this.props.modalData, {date: this.state.modalDate});
        //Don't open modal for a day without any deadlines:
        if (tooltip === undefined) {
            return <span />;
        }

        return (
            <Modal
                title={"Assignments due " + moment(tooltip.date).format('dddd, MMMM Do YYYY')}
                onRequestHide={this.closeModal}
            >
                <div className="modal-body">
                {tooltip.tooltip}
                </div>
                <div className="modal-footer">
                    <button className="btn btn-default" onClick={this.closeModal}>Close</button>
                </div>
            </Modal>
        )

    }
});

React.render(
    <FilteredCalendar />,
    document.getElementById('calendar')
);