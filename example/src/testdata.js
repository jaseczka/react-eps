var _ = require('lodash');
var Label = require('react-bootstrap').Label;

assignments = [
    {
        "id":880578,
        "course":1411515,
        "courseId":1411515,
        "canvasInstance":null,
        "assignmentId":6428407,
        "assignmentName":"CW: Vocab/Grammar Quiz 10",
        "assignmentDueAt":"2015-01-09T20:15:00.000Z",
        "assignmentGroupId":1748866,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"on_paper",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EWords through List 10 and Grammar Bite can vs. may\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411515/assignments/6428407",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":"https://eastsideprep.instructure.com/courses/1411515/assignments/6428407/submissions/3091727?preview=1",
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":"--- []\n",
        "derivedStatus":"upcoming",
        "lastCheckedAt":"2015-01-09T09:18:04.544Z",
        "submissionLate":false,
        "yamlMagicComments":"--- []\n",
        "submissionType":"offline"
    },
    {
        "id":891679,
        "course":1411565,
        "courseId":1411565,
        "canvasInstance":null,
        "assignmentId":6459492,
        "assignmentName":"CW: Work in packet and work on program",
        "assignmentDueAt":"2015-01-06T17:30:00.000Z",
        "assignmentGroupId":2133359,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":null,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_quiz",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EIn class, some time will be spent working on the problems in the packet, starting with problem 9 on page 26, and some time will be spent working on your program.  At the end of class, take this survey to tell me your progress.\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411565/assignments/6459492",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"missing",
        "lastCheckedAt":"2015-01-09T09:32:44.119Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":891165,
        "course":1411543,
        "courseId":1411543,
        "canvasInstance":null,
        "assignmentId":6457086,
        "assignmentName":"Responsible action 3",
        "assignmentDueAt":"2015-01-10T07:59:59.000Z",
        "assignmentGroupId":2122695,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":null,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"none",
        "assignmentUnlockDate":null,
        "assignmentDescription":null,
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411543/assignments/6457086",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"n/a",
        "lastCheckedAt":"2015-01-09T09:25:40.127Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"ungraded"
    },
    {
        "id":895262,
        "course":1411543,
        "courseId":1411543,
        "canvasInstance":null,
        "assignmentId":6577968,
        "assignmentName":"Progress measures",
        "assignmentDueAt":"2015-01-07T07:59:59.000Z",
        "assignmentGroupId":2122691,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_text_entry",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003ECollect data for your progress measures and submit this information. Remember to list each of your check measures and your assessment. Let me know if you have any questions.\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411543/assignments/6577968",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"missing",
        "lastCheckedAt":"2015-01-09T09:25:40.604Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":886716,
        "course":1411565,
        "courseId":1411565,
        "canvasInstance":null,
        "assignmentId":6417947,
        "assignmentName":"HW:  Intro to Alg 8 Continue the packet",
        "assignmentDueAt":"2015-01-08T21:50:00.000Z",
        "assignmentGroupId":1720158,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_upload",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EContinue working on the packet starting on problem 7 or where you left off for 30 minutes, or until you finish it. Upload your work you did for this assignment.\u003C/p\u003E\r\n\u003Cp\u003E(This was changed from a text-entry assignment to a file-upload assignment at 9:25 am on Wed.)\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411565/assignments/6417947",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"missing",
        "lastCheckedAt":"2015-01-09T09:32:44.009Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":895261,
        "course":1411543,
        "courseId":1411543,
        "canvasInstance":null,
        "assignmentId":6577324,
        "assignmentName":"Lesson Plan",
        "assignmentDueAt":"2015-01-09T07:59:00.000Z",
        "assignmentGroupId":2163826,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_text_entry|online_upload",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EEach group will submit a lesson plan for your day. Please follow the format below and respond with any questions.\u003C/p\u003E\r\n\u003Cp\u003E \u003C/p\u003E\r\n\u003Cp style=\"text-align: center;\"\u003ELesson Plan\u003C/p\u003E\r\n\u003Cp style=\"text-align: left;\"\u003ENames:\u003C/p\u003E\r\n\u003Cp style=\"text-align: left;\"\u003EPeriod:\u003C/p\u003E\r\n\u003Cp style=\"text-align: left;\"\u003ESport/activity:\u003C/p\u003E\r\n\u003Cp style=\"text-align: left;\"\u003EDate:\u003C/p\u003E\r\n\u003Cp style=\"text-align: left;\"\u003E \u003C/p\u003E\r\n\u003Cp style=\"text-align: left;\"\u003EI. Logistic information\u003C/p\u003E\r\n\u003Col\u003E\r\n\u003Cli\u003Eequipment needed\u003C/li\u003E\r\n\u003Cli\u003ELocation\u003C/li\u003E\r\n\u003Cli\u003EAnything else you need or need me to know.\u003C/li\u003E\r\n\u003C/ol\u003E\r\n\u003Cp\u003EII. Plan\u003C/p\u003E\r\n\u003Col\u003E\r\n\u003Cli\u003EWarm-up activities\u003C/li\u003E\r\n\u003Cli\u003Eintroduce skills \u0026amp; drills\u003C/li\u003E\r\n\u003Cli\u003EGames, activities, exercises etc.\u003C/li\u003E\r\n\u003C/ol\u003E\r\n\u003Cp\u003EIII. Peer \u0026amp; self reflection time.\u003C/p\u003E\r\n\u003Cp\u003E \u003C/p\u003E\r\n\u003Cp\u003E \u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411543/assignments/6577324",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"missing",
        "lastCheckedAt":"2015-01-09T09:25:40.573Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":894439,
        "course":1411515,
        "courseId":1411515,
        "canvasInstance":null,
        "assignmentId":6551638,
        "assignmentName":"CW: Creative Writing 13",
        "assignmentDueAt":"2015-01-09T21:40:00.000Z",
        "assignmentGroupId":2113641,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_text_entry",
        "assignmentUnlockDate":null,
        "assignmentDescription":"",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411515/assignments/6551638",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":"https://eastsideprep.instructure.com/courses/1411515/assignments/6551638/submissions/3091727?preview=1",
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":"--- []\n",
        "derivedStatus":"upcoming",
        "lastCheckedAt":"2015-01-09T09:18:07.723Z",
        "submissionLate":false,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":891171,
        "course":1411543,
        "courseId":1411543,
        "canvasInstance":null,
        "assignmentId":6457094,
        "assignmentName":"Leadership 3",
        "assignmentDueAt":"2015-01-10T07:59:59.000Z",
        "assignmentGroupId":2122696,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":null,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"none",
        "assignmentUnlockDate":null,
        "assignmentDescription":null,
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411543/assignments/6457094",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"n/a",
        "lastCheckedAt":"2015-01-09T09:25:40.296Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"ungraded"
    },
    {
        "id":891176,
        "course":1411543,
        "courseId":1411543,
        "canvasInstance":null,
        "assignmentId":6457121,
        "assignmentName":"Preparation 3",
        "assignmentDueAt":"2015-01-10T07:59:59.000Z",
        "assignmentGroupId":2122697,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":null,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"none",
        "assignmentUnlockDate":null,
        "assignmentDescription":null,
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411543/assignments/6457121",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"n/a",
        "lastCheckedAt":"2015-01-09T09:25:40.459Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"ungraded"
    },
    {
        "id":852523,
        "course":1411527,
        "courseId":1411527,
        "canvasInstance":null,
        "assignmentId":6284227,
        "assignmentName":"MA: Surv. of Choc. Write-Up ",
        "assignmentDueAt":"2015-01-16T20:15:00.000Z",
        "assignmentGroupId":2083506,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_upload",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EThis is due at the start of class on Friday. The assignment instructions will be attached here once they have been handed out in class.  \u003C/p\u003E\r\n\u003Cp\u003EPay close attention to the assignment expectations!  You can also use the attached template (attached after it's been reviewed in class) if you would find it helpful.\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411527/assignments/6284227",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"upcoming",
        "lastCheckedAt":"2015-01-09T09:38:55.663Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":896919,
        "course":1411521,
        "courseId":1411521,
        "canvasInstance":null,
        "assignmentId":6595777,
        "assignmentName":"HW: Plateau and Coastal Tribes articles and notes",
        "assignmentDueAt":"2015-01-14T16:00:00.000Z",
        "assignmentGroupId":1704415,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_upload",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003E\u003Cspan style=\"font-size: 14px;\"\u003E\u003Ca id=\"\" class=\" instructure_file_link\" title=\"1.14.2015 Coastal and Plateau Guided Notes.docx\" href=\"https://eastsideprep.instructure.com/courses/1411521/files/62598872/download?verifier=J4EAuNoErxOO05b0RA5Er6sdnFK8rKhHVeamkRhO\u0026amp;wrap=1\" data-api-endpoint=\"https://eastsideprep.instructure.com/api/v1/files/62598872\" data-api-returntype=\"File\"\u003E1.14.2015 Coastal and Plateau Guided Notes.docx\u003C/a\u003E\u003C/span\u003E\u003C/p\u003E\r\n\u003Cp\u003E\u003Cspan style=\"font-size: 14px;\"\u003ERead these articles and complete the guided notes below:\u003C/span\u003E\u003C/p\u003E\r\n\u003Cp\u003E\u003Cspan style=\"font-size: 14px;\"\u003ECoastal \u003C/span\u003E\u003Ca style=\"font-size: 14px;\" href=\"http://school.eb.com/levels/high/article/117305\"\u003Ehttp://school.eb.com/levels/high/article/117305\u003C/a\u003E\u003Cspan style=\"font-size: 14px;\"\u003E  \u003C/span\u003E\u003C/p\u003E\r\n\u003Cp\u003E\u003Cspan style=\"font-size: 14px;\"\u003EPlateau \u003C/span\u003E\u003Ca style=\"font-size: 14px;\" href=\"http://school.eb.com/levels/high/article/117307\"\u003Ehttp://school.eb.com/levels/high/article/117307\u003C/a\u003E\u003Cspan style=\"font-size: 14px;\"\u003E \u003C/span\u003E\u003C/p\u003E\r\n\u003Cp\u003E*Username: eps05          Password: prepschool\u003C/p\u003E\r\n\u003Cp\u003E*The Eastside Prep Library subscribes to several online databases for the benefit of our students and faculty. The licenses for these subscriptions are restricted in their use and therefore can be used only by the members of the Eastside Prep community, including faculty, administration, students, and parents. Please honor the limitations placed on these resources and do not distribute the usernames and passwords to anyone outside of the Eastside Prep community.\u003C/p\u003E\r\n\u003Cp\u003E\u003Cspan style=\"font-size: 14px;\"\u003EBelow is the note table for [Coastal and Plateau Tribes]. By the end of the unit a completed notes template would have:\u003C/span\u003E\u003C/p\u003E\r\n\u003Cul\u003E\r\n\u003Cli\u003E4-5 bullet points in each section.\u003C/li\u003E\r\n\u003Cli\u003E[Expand the bullet points that end with ellipses (…)]\u003C/li\u003E\r\n\u003C/ul\u003E\r\n\u003Ctable border=\"1\" cellspacing=\"0\" cellpadding=\"0\"\u003E\r\n\u003Ctbody\u003E\r\n\u003Ctr\u003E\r\n\u003Ctd valign=\"top\" width=\"150\"\u003E\r\n\u003Cp\u003EMajor Event or Movement\u003C/p\u003E\r\n\u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"188\"\u003E\r\n\u003Cp\u003EImportant Dates\u003C/p\u003E\r\n\u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"233\"\u003E\r\n\u003Cp\u003EEssential People/Factors/ Hallmarks\u003C/p\u003E\r\n\u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"255\"\u003E\r\n\u003Cp\u003EImmediate Impact(s) on Seattle\u003C/p\u003E\r\n\u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"255\"\u003E\r\n\u003Cp\u003ELong-term Impacts on Seattle\u003C/p\u003E\r\n\u003C/td\u003E\r\n\u003C/tr\u003E\r\n\u003Ctr\u003E\r\n\u003Ctd valign=\"top\" width=\"150\"\u003E\r\n\u003Cp\u003ECoastal Tribes\u003C/p\u003E\r\n\u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"188\"\u003E\r\n\u003Cul\u003E\r\n\u003Cli\u003E1700s Europeans appear in area\u003C/li\u003E\r\n\u003Cli\u003E1741-1799: Russians encounter Tlingit\u003C/li\u003E\r\n\u003Cli\u003E1780-1900: Disease\u003C/li\u003E\r\n\u003Cli\u003EFind at least 1 other important date\u003C/li\u003E\r\n\u003Cli\u003EFishing…\u003C/li\u003E\r\n\u003Cli\u003EHousing…\u003C/li\u003E\r\n\u003Cli\u003EClothing…\u003C/li\u003E\r\n\u003Cli\u003EWoodworking…\u003C/li\u003E\r\n\u003Cli\u003ESocial structure…\u003C/li\u003E\r\n\u003Cli\u003EVision quest…\u003C/li\u003E\r\n\u003Cli\u003ESmallpox…\u003C/li\u003E\r\n\u003Cli\u003EEarly Euro-American pioneers see/eat smoked salmon\u003C/li\u003E\r\n\u003Cli\u003EFur trade\u003C/li\u003E\r\n\u003Cli\u003EAdd more here…\u003C/li\u003E\r\n\u003Cli\u003ESalmon part of local cuisine today\u003C/li\u003E\r\n\u003Cli\u003ENative art popular in Seattle today\u003C/li\u003E\r\n\u003Cli\u003EAdd more here…\u003C/li\u003E\r\n\u003C/ul\u003E\r\n\u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"233\"\u003E \u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"255\"\u003E \u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"255\"\u003E \u003C/td\u003E\r\n\u003C/tr\u003E\r\n\u003Ctr\u003E\r\n\u003Ctd valign=\"top\" width=\"150\"\u003E\r\n\u003Cp\u003EPlateau Tribes\u003C/p\u003E\r\n\u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"188\"\u003E\r\n\u003Cul\u003E\r\n\u003Cli\u003E1700s receive some horses\u003C/li\u003E\r\n\u003Cli\u003E1805-1806: Lewis and Clark contact\u003C/li\u003E\r\n\u003Cli\u003E1800s fur trade\u003C/li\u003E\r\n\u003Cli\u003EFind at least 1 other important date\u003C/li\u003E\r\n\u003Cli\u003EFood…\r\n\u003Cul\u003E\r\n\u003Cli\u003EBerries…\u003C/li\u003E\r\n\u003C/ul\u003E\r\n\u003C/li\u003E\r\n\u003Cli\u003EHousing…\u003C/li\u003E\r\n\u003Cli\u003ETrade and contact with other tribes…\u003C/li\u003E\r\n\u003Cli\u003EHunting technology…\u003C/li\u003E\r\n\u003Cli\u003ESanpoil political structure…\u003C/li\u003E\r\n\u003Cli\u003EFlathead political structure…\u003C/li\u003E\r\n\u003Cli\u003ESharing resources…\u003C/li\u003E\r\n\u003Cli\u003EAnimism…\u003C/li\u003E\r\n\u003Cli\u003EShamans…\u003C/li\u003E\r\n\u003Cli\u003EProphet Dance…\u003C/li\u003E\r\n\u003Cli\u003EReservations…\u003C/li\u003E\r\n\u003Cli\u003EChief Joseph…\u003C/li\u003E\r\n\u003Cli\u003EWillingness to trade connected them to early pioneers\u003C/li\u003E\r\n\u003Cli\u003EReservation system became a part of Washington State\u003C/li\u003E\r\n\u003Cli\u003EAdd more here…\u003C/li\u003E\r\n\u003Cli\u003ECourt hearings for land disputes and treaties\u003C/li\u003E\r\n\u003Cli\u003ESalmon run studied, protected, and celebrated today\u003C/li\u003E\r\n\u003Cli\u003EAdd more here…\u003C/li\u003E\r\n\u003C/ul\u003E\r\n\u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"233\"\u003E \u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"255\"\u003E \u003C/td\u003E\r\n\u003Ctd valign=\"top\" width=\"255\"\u003E \u003C/td\u003E\r\n\u003C/tr\u003E\r\n\u003C/tbody\u003E\r\n\u003C/table\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411521/assignments/6595777",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"upcoming",
        "lastCheckedAt":"2015-01-09T09:39:21.623Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":874190,
        "course":1411527,
        "courseId":1411527,
        "canvasInstance":null,
        "assignmentId":6284228,
        "assignmentName":"CW: Classifying Shoes",
        "assignmentDueAt":"2015-01-21T23:15:00.000Z",
        "assignmentGroupId":2083505,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_upload",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cdiv class=\"description user_content student-version enhanced\"\u003E\r\n\u003Cp\u003EYou will start this lab in class on Friday of this week and finish it next Wednesday\u003C/p\u003E\r\n\u003Cp\u003EAs a \u003Cspan style=\"color: #ff0000;\"\u003Egroup\u003C/span\u003E, you must turn your key in on paper.\u003C/p\u003E\r\n\u003Cp\u003EAs \u003Cspan style=\"color: #ff0000;\"\u003Eindividuals\u003C/span\u003E, you must submit the following here:\u003C/p\u003E\r\n\u003Cp\u003E1.  Your list of features\u003C/p\u003E\r\n\u003Cp\u003E2.  Your table\u003C/p\u003E\r\n\u003Cp\u003E3.  The answers to the two analysis questions on p. 221\u003C/p\u003E\r\n\u003C/div\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411527/assignments/6284228",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":"https://eastsideprep.instructure.com/courses/1411527/assignments/6284228/submissions/3091727?preview=1",
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":"--- []\n",
        "derivedStatus":"upcoming",
        "lastCheckedAt":"2015-01-09T09:12:13.004Z",
        "submissionLate":false,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":874185,
        "course":1411527,
        "courseId":1411527,
        "canvasInstance":null,
        "assignmentId":6283500,
        "assignmentName":"Responsible Action 1/14-1/16",
        "assignmentDueAt":"2015-01-17T00:00:00.000Z",
        "assignmentGroupId":2082833,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"none",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EThis grade is based on your participation, preparedness and contributions to the learning environment.  It also reflects the timeliness of your submitted work, so missing or late assignments will impact it.  As part of the process, you may be expected to reflect on how the week went for you.  If so, you will be given time in Friday's class to do this.  Your grade is based on my observations, and my comments will let you know my thoughts about your self-reflection.\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411527/assignments/6283500",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"n/a",
        "lastCheckedAt":"2015-01-09T09:38:55.378Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"ungraded"
    },
    {
        "id":874192,
        "course":1411527,
        "courseId":1411527,
        "canvasInstance":null,
        "assignmentId":6284231,
        "assignmentName":"CW: Shape Island",
        "assignmentDueAt":"2015-01-14T23:15:00.000Z",
        "assignmentGroupId":2083505,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_text_entry",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EYou will do this activity in class on Wednesday .  Your drawings will be done on paper and you have the option of submitting your answers to all of the questions in the text box.\u003C/p\u003E\r\n\u003Cp\u003EPlease note:  if you are submitting the entire lab on paper, the only way to remove this assignment from your To Do list is to submit an entry in the text box.  Something like, \"I did this on paper\" will be fine.  If you don't do this, the assignment will stay on your to do list even when it has been graded!\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411527/assignments/6284231",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":"https://eastsideprep.instructure.com/courses/1411527/assignments/6284231/submissions/3091727?preview=1",
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":"--- []\n",
        "derivedStatus":"upcoming",
        "lastCheckedAt":"2015-01-09T09:12:12.663Z",
        "submissionLate":false,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":874184,
        "course":1411527,
        "courseId":1411527,
        "canvasInstance":null,
        "assignmentId":6283499,
        "assignmentName":"Responsible Action 1/7-1/9",
        "assignmentDueAt":"2015-01-10T00:00:00.000Z",
        "assignmentGroupId":2082833,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"none",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EThis grade is based on your participation, preparedness and contributions to the learning environment.  It also reflects the timeliness of your submitted work, so missing or late assignments will impact it.  As part of the process, you may be expected to reflect on how the week went for you.  If so, you will be given time in Friday's class to do this.  Your grade is based on my observations, and my comments will let you know my thoughts about your self-reflection.\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411527/assignments/6283499",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"n/a",
        "lastCheckedAt":"2015-01-09T09:38:55.350Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"ungraded"
    },
    {
        "id":891159,
        "course":1411543,
        "courseId":1411543,
        "canvasInstance":null,
        "assignmentId":6457078,
        "assignmentName":"Thinking 3",
        "assignmentDueAt":"2015-01-10T07:59:59.000Z",
        "assignmentGroupId":2122694,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":null,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"none",
        "assignmentUnlockDate":null,
        "assignmentDescription":null,
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411543/assignments/6457078",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"n/a",
        "lastCheckedAt":"2015-01-09T09:25:39.961Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"ungraded"
    },
    {
        "id":897311,
        "course":1411565,
        "courseId":1411565,
        "canvasInstance":null,
        "assignmentId":6610099,
        "assignmentName":"HW: Intro to Alg 8: FINISH!!!! the packet",
        "assignmentDueAt":"2015-01-13T16:00:00.000Z",
        "assignmentGroupId":1720158,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_upload",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EYou should be able to finish the packet with 40 minutes of work.  If it does take you longer than 40 minutes, you may stop, but do feel free to continue until you finish it.  Whether you finish it or not for this assignment, you will be responsible for knowing how to solve all of the problems.  If you've lost your packet, it is available under Files, Handouts, \u003Ca class=\"instructure_file_link instructure_scribd_file \" title=\"14-12-04 Orangeade Pizzas and Problems.pdf\" href=\"https://eastsideprep.instructure.com/courses/1411565/files/61131716/download?verifier=DLk9XmFWd332NjoRfuyjGhIauW7WuSIkBqlV1DHn\u0026amp;wrap=1\" data-api-endpoint=\"https://eastsideprep.instructure.com/api/v1/files/61131716\" data-api-returntype=\"File\"\u003E14-12-04 Orangeade Pizzas and Problems.pdf\u003C/a\u003E.\u003C/p\u003E\r\n\u003Cp\u003EUpload your electronic paper or a legible copy of your work.\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411565/assignments/6610099",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":null,
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":null,
        "derivedStatus":"upcoming",
        "lastCheckedAt":"2015-01-09T09:32:44.195Z",
        "submissionLate":null,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":894172,
        "course":1411530,
        "courseId":1411530,
        "canvasInstance":null,
        "assignmentId":6544160,
        "assignmentName":"HW: Study the file “Indirect object pronouns and professions”",
        "assignmentDueAt":"2015-01-09T23:00:00.000Z",
        "assignmentGroupId":2110960,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"not_graded",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003EHW: Review and study the file “Indirect object pronouns and professions”.\u003C/p\u003E\r\n\u003Cp\u003EIn class, you will have the opportunity to correct your mistakes in your last MA using the information in this file.\u003C/p\u003E\r\n\u003Cp\u003EYou will be able to earn up to one third of the grade, if you correct properly your mistakes.\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411530/assignments/6544160",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":"https://eastsideprep.instructure.com/courses/1411530/assignments/6544160/submissions/3091727?preview=1",
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":"--- []\n",
        "derivedStatus":"n/a",
        "lastCheckedAt":"2015-01-09T09:15:21.471Z",
        "submissionLate":false,
        "yamlMagicComments":"--- []\n",
        "submissionType":"ungraded"
    },
    {
        "id":894188,
        "course":1411530,
        "courseId":1411530,
        "canvasInstance":null,
        "assignmentId":6544329,
        "assignmentName":"Reading: ¿Dónde están mis gafas? ",
        "assignmentDueAt":"2015-01-14T23:00:00.000Z",
        "assignmentGroupId":2111130,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":null,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"online_quiz",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003ELee el siguiente texto y contesta las preguntas (read the following text and answer the questions)\u003C/p\u003E\n\u003Cp\u003E \u003C/p\u003E\n\u003Cp\u003E¿Dónde están mis gafas?\u003C/p\u003E\n\u003Cp\u003E \u003C/p\u003E\n\u003Cp\u003ESoy una persona un poco desordenada. Mi habitación siempre tiene muchos trastos por todas partes y, por eso, a veces no encuentro lo que busco. Creo que el problema es que necesito más espacio. Mi cuarto mide solo 20 metros cuadrados y eso es muy poco.\u003C/p\u003E\n\u003Cp\u003ELa cama está situada en el centro de la habitación. En frente está el armario; es un armario muy grande de color blanco. Dentro tiene tres cajones enormes y muchas perchas. En los cajones pongo los jerséis y la ropa pequeña.\u003C/p\u003E\n\u003Cp\u003EAl lado de la cama tengo una mesita de noche y sobre la mesita de noche siempre hay muchas cosas: un libro, una botella de agua, rotuladores, lápices, pendientes, collares, una agenda, crema hidratante y una lamparita muy mona.\u003C/p\u003E\n\u003Cp\u003EA la izquierda del armario está mi escritorio. Sobre el escritorio tengo el ordenador portátil y un flexo. Debajo de mi escritorio hay una papelera. Aunque es pequeña, mi habitación me gusta mucho porque tiene mis colores preferidos: el blanco y el azul. Pero entre tantas cosas... ¡¿dónde están mis gafas?! Es muy difícil encontrarlas.\u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411530/assignments/6544329",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":"https://eastsideprep.instructure.com/courses/1411530/assignments/6544329/submissions/3091727?preview=1",
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":"--- []\n",
        "derivedStatus":"upcoming",
        "lastCheckedAt":"2015-01-09T09:15:21.957Z",
        "submissionLate":false,
        "yamlMagicComments":"--- []\n",
        "submissionType":"online"
    },
    {
        "id":894246,
        "course":1411530,
        "courseId":1411530,
        "canvasInstance":null,
        "assignmentId":6544211,
        "assignmentName":"HW: sentence of the day",
        "assignmentDueAt":"2015-01-14T23:00:00.000Z",
        "assignmentGroupId":2111137,
        "assignmentGroupName":null,
        "assignmentGroupWeight":null,
        "assignmentTurnitinEnabled":false,
        "assignmentMuted":false,
        "assignmentPublished":true,
        "assignmentSubmissionTypes":"not_graded",
        "assignmentUnlockDate":null,
        "assignmentDescription":"\u003Cp\u003ESentence of the day: Fill up the gaps: En mi casa, ________________________ está al lado de _________________. \u003C/p\u003E",
        "assignmentHtmlUrl":"https://eastsideprep.instructure.com/courses/1411530/assignments/6544211",
        "assignmentGradingStandardId":null,
        "submissionAttempt":null,
        "submissionGrade":null,
        "submissionHtmlUrl":"https://eastsideprep.instructure.com/courses/1411530/assignments/6544211/submissions/3091727?preview=1",
        "submissionScore":null,
        "submissionSubmittedAt":null,
        "user":3091727,
        "userId":3091727,
        "submissionComments":"--- []\n",
        "derivedStatus":"n/a",
        "lastCheckedAt":"2015-01-09T09:15:25.670Z",
        "submissionLate":false,
        "yamlMagicComments":"--- []\n",
        "submissionType":"ungraded"
    }
];

var getAssignments = function(dateFrom, dateTo, filters, assignments) {
    dateFrom = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate());
    dateTo = new Date(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
    var dateKey = "assignmentDueAt";

    var cleanedData = _.filter(assignments, function(a) {
        var date = new Date(a[dateKey]);
        return (date >= dateFrom && date <= dateTo);
    }, this);

    if (filters != null) {
        cleanedData = _.filter(cleanedData, function(a) {
            //all filter tests must return true
            for (var filter = 0; filter < filters.length; ++filter) {
                if (!filters[filter].test(a)) {
                    return false;
                }
            }
            return true;
        });
    }

    var groupedData = _.groupBy(cleanedData, function(a) {
        var date = new Date(a[dateKey]);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }, this);

    var aggregatedData = _.countBy(cleanedData, function(a) {
        var date = new Date(a[dateKey]);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }, this);

    var data = [];
    var tooltips = [];

    for (var key in aggregatedData) {
        if (aggregatedData.hasOwnProperty(key)) {
            data.push({
                date: new Date(key),
                value: aggregatedData[key]
            });
        }
    }

    for (var key in groupedData) {
        if (groupedData.hasOwnProperty(key)) {
            var tooltip = "";

            for (var i = 0; i < groupedData[key].length; ++i) {
                tooltip += groupedData[key][i].assignmentName;
                tooltip += "<br />";
            }

            tooltips.push({
                date: new Date(key),
                tooltip: tooltip
            })
        }
    }

    data.sort(function(lhs, rhs) {
        return lhs.date.getTime() - rhs.date.getTime();
    });

    return {
        data: data,
        tooltips: tooltips
    }
};

var getData = function(dateFrom, dateTo, filters) {
    return getAssignments(dateFrom, dateTo, filters, assignments);
};

var createBasicFilters = function(data, properties, dictionary) {
    if (!data || !properties)
        return null;

    dictionary = dictionary || {};

    var filters = [];

    var test = function(property, options, assignment) {
        if (assignment.hasOwnProperty(property)) {
            var testedValue = assignment[property];
            //at least one of the options tests must return true
            for (var i = 0; i < options.length; ++i) {
                if (options[i].include && options[i].test(testedValue)) {
                    return true;
                }
            }
            return false;
        }
    };

    for (var i = 0; i < properties.length; ++i) {
        var allValues = _.pluck(data, properties[i]);
        var uniqueValues = _.uniq(allValues);
        var options = [];
        _.forEach(uniqueValues, function(value) {
            options.push({
                displayName: String(value),
                test: function(that) {
                    return (that === value);
                },
                include: true
            })
        });
        var name = properties[i];
        if (dictionary.hasOwnProperty(properties[i])) {
            name = dictionary[properties[i]];
        }
        var filter = {
            displayName: name,
            property: properties[i],
            test: function(assignment) {
                return test(this.property, this.options, assignment);
            },
            options: options
        };
        filters.push(filter);
    }

    return filters;
};

var createStartsWithFilter = function(data, property, displayName, startOptions) {
    var test = function(assignment) {
        if (assignment.hasOwnProperty(property)) {
            var testedValue = assignment[property];
            //at least one of the options tests must return true
            for (var i = 0; i < this.options.length; ++i) {
                if (this.options[i].include && this.options[i].test(testedValue)) {
                    return true;
                }
            }
            return false;
        }
    };

    var options = startOptions.map(function(str) {
        return {
            displayName: str,
            include: true,
            test: function(text) {
                return (text.indexOf(str) === 0);
            }
        }
    });

    options.push({
        displayName: "other",
        include: true,
        test: function(text) {
            for (var i = 0; i < startOptions.length; ++i) {
                if (text.indexOf(startOptions[i]) === 0)
                    return false;
            }
            return true;
        }
    });

    return {
        displayName: displayName,
        property: property,
        test: test,
        options: options
    }
};

var getFilters = function() {
    var filters = createBasicFilters(
        assignments,
        ["derivedStatus", "submissionLate", "submissionType", "assignmentSubmissionTypes"],
        {
            "derivedStatus": "Status",
            "submissionType": "Submission Type"
        }
    );
    var regexFilter = createStartsWithFilter(
        assignments,
        "assignmentName",
        "Assignment Type",
        ["CW", "HW", "MA"]
    );
    filters.push(regexFilter);
    return filters;
};

var getRawData = function() {
    return assignments;
};

var createTooltip = function(assignments) {
    var lines = assignments.map(function(a) {
        var label = null;
        if (a.derivedStatus !== "n/a") {
            label = <Label bsStyle="default">{a.derivedStatus}</Label>;
        }
        return <li key={'id' + a.id}><a href={a.assignmentHtmlUrl}>{a.assignmentName}</a>{label}</li>
    });

    return <ul>{lines}</ul>
};

var aggregateData = function(rawData) {
    var dateKey = "assignmentDueAt";

    var groupedData = _.groupBy(rawData, function(a) {
        var date = new Date(a[dateKey]);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }, this);

    var data = [];
    var tooltips = [];

    for (var key in groupedData) {
        if (groupedData.hasOwnProperty(key)) {
            data.push({
                date: new Date(key),
                value: groupedData[key].length
            });

            tooltips.push({
                date: new Date(key),
                tooltip: createTooltip(groupedData[key])
            });
        }
    }


    data.sort(function(lhs, rhs) {
        return lhs.date.getTime() - rhs.date.getTime();
    });

    return {
        data: data,
        tooltips: tooltips
    }
};

exports.getRawData = getRawData;
exports.getData = getData;
exports.getFilters = getFilters;
exports.aggregateData = aggregateData;