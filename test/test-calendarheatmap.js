var moment = require('moment');
var React = require('react/addons');
var TestUtils = React.addons.TestUtils;
var CalendarHeatMap = require('../lib/calendarheatmap');
var Day = CalendarHeatMap.__get__('Day');
var XAxis = CalendarHeatMap.__get__('XAxis');
var YAxis = CalendarHeatMap.__get__('YAxis');
var DataSeries = CalendarHeatMap.__get__('DataSeries');


describe('Day', function() {
    var day;

    beforeEach(function() {
        var date = new Date(2015, 0, 1);

        day = TestUtils.renderIntoDocument(
            <Day
                handleClick={null}
                date={date}
                textContent={date.getDate().toString()}
                x={0}
                y={0}
                fill="white"
                stroke="black"
                width={50}
                height={50}
                value={1}
                counterEnabled={false}
            />
        );
    });

    it('renders element with chm-day class', function() {
        var dayClass = TestUtils.findRenderedDOMComponentWithClass(day, 'chm-day');
        expect(dayClass).toBeDefined();
        expect(TestUtils.isDOMComponentElement);
    });
});

describe('XAxis', function() {
    var xaxis;

    beforeEach(function() {
        xaxis = TestUtils.renderIntoDocument(
            <XAxis
                width={200}
                margins={{top: 10, right: 10, bottom: 10, left: 10}}
            />
        );
    });

    it('renders element with xaxis class', function() {
        var xaxisClass = TestUtils.findRenderedDOMComponentWithClass(xaxis, 'xaxis');
        expect(xaxisClass).toBeDefined();
    });

    it('rendenrs 7 labels for weekdays', function() {
        var labels = TestUtils.scryRenderedDOMComponentsWithClass(xaxis, 'weekday-title');
        expect(labels.length).toEqual(7);
    });
    
});

describe('YAxis', function() {

});

describe('DataSeries', function() {

});

describe('CalendarHeatMap component', function() {

});