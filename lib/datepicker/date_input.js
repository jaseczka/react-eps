/** @jsx React.DOM */

var React = require('react');
var moment = require('moment');
var DateUtil = require('./util/date');

var DateInput = React.createClass({displayName: "DateInput",

  getDefaultProps: function() {
    return {
      dateFormat: 'YYYY-MM-DD'
    };
  },

  getInitialState: function() {
    return {
      value: this.safeDateFormat(this.props.date)
    };
  },

  componentDidMount: function() {
    this.toggleFocus(this.props.focus);
  },

  componentWillReceiveProps: function(newProps) {
    this.toggleFocus(newProps.focus);

    this.setState({
      value: this.safeDateFormat(newProps.date)
    });
  },

  toggleFocus: function(focus) {
    if (focus) {
      this.refs.input.getDOMNode().focus();
    } else {
      this.refs.input.getDOMNode().blur();
    }
  },

  handleChange: function(event) {
    //var date = moment(event.target.value, this.props.dateFormat, true);

    this.setState({
      value: event.target.value
    });

    //if (this.isValueAValidDate(event.target.value)) {
    //  this.props.setSelected(new DateUtil(date));
    //}
  },

  safeDateFormat: function(date) {
    return !! date ? date.format(this.props.dateFormat) : null;
  },

  isValueAValidDate: function(value) {
    var date = moment(value, this.props.dateFormat, true);

    return date.isValid();
  },

  handleKeyDown: function(event) {
    switch(event.key) {
    case "Enter":
      event.preventDefault();
      this.setChangedIfValid();
      this.props.handleEnter();
      break;
    }
  },

  handleClick: function(event) {
    this.props.handleClick(event);
  },

  setChangedIfValid: function() {
    var date = moment(this.state.value, this.props.dateFormat, true);
    if (this.isValueAValidDate(this.state.value)) {
      this.props.setSelected(new DateUtil(date));
    }
  },

  handleBlur: function(event) {
    this.setChangedIfValid();
  },

  render: function() {
    return React.createElement("input", {
      ref: "input", 
      type: "text", 
      value: this.state.value, 
      onClick: this.handleClick, 
      onKeyDown: this.handleKeyDown, 
      onFocus: this.props.onFocus, 
      onChange: this.handleChange, 
      onBlur: this.handleBlur, 
      className: "datepicker__input", 
      placeholder: this.props.placeholderText});
  }
});

module.exports = DateInput;
