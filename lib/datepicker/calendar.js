/** @jsx React.DOM */

var React = require('react');
var moment = require('moment');
var Day = require('./day');
var DateUtil = require('./util/date');

var Calendar = React.createClass({displayName: "Calendar",
  mixins: [require('react-onclickoutside')],

  handleClickOutside: function() {
    this.props.hideCalendar();
  },

  getInitialState: function() {
    return {
      date: new DateUtil(this.props.selected).safeClone(moment())
    };
  },

  componentWillReceiveProps: function(nextProps) {
    // When the selected date changed
    if (nextProps.selected !== this.props.selected) {
      this.setState({
        date: new DateUtil(nextProps.selected).clone()
      });
    }
  },

  increaseMonth: function() {
    this.setState({
      date: this.state.date.addMonth()
    });
  },

  decreaseMonth: function() {
    this.setState({
      date: this.state.date.subtractMonth()
    });
  },

  weeks: function() {
    return this.state.date.mapWeeksInMonth(this.renderWeek);
  },

  handleDayClick: function(day) {
    this.props.onSelect(day);
  },

  renderWeek: function(weekStart, key) {
    if(! weekStart.weekInMonth(this.state.date)) {
      return;
    }

    return (
      React.createElement("div", {key: key}, 
        this.days(weekStart)
      )
    );
  },

  renderDay: function(day, key) {
    var minDate = new DateUtil(this.props.minDate).safeClone(),
        maxDate = new DateUtil(this.props.maxDate).safeClone(),
        disabled = day.isBefore(minDate) || day.isAfter(maxDate);

    return (
      React.createElement(Day, {
        key: key, 
        day: day, 
        date: this.state.date, 
        onClick: this.handleDayClick.bind(this, day), 
        selected: new DateUtil(this.props.selected), 
        disabled: disabled})
    );
  },

  days: function(weekStart) {
    return weekStart.mapDaysInWeek(this.renderDay);
  },

  weekdays: function() {
    var days = [0, 1, 2, 3, 4, 5, 6];
    var firstDay = moment().startOf('week');
    days = days.map(function(day) {
      var weekday = moment(firstDay).add(day, 'day');
      return React.createElement("div", {className: "datepicker__day"}, weekday.format('dd'));
    });
    return days
  },

  render: function() {
    return (
      React.createElement("div", {className: "datepicker"}, 
        React.createElement("div", {className: "datepicker__triangle"}), 
        React.createElement("div", {className: "datepicker__header"}, 
          React.createElement("a", {className: "datepicker__navigation datepicker__navigation--previous", 
              onClick: this.decreaseMonth}
          ), 
          React.createElement("span", {className: "datepicker__current-month"}, 
            this.state.date.format("MMMM YYYY")
          ), 
          React.createElement("a", {className: "datepicker__navigation datepicker__navigation--next", 
              onClick: this.increaseMonth}
          ), 
          React.createElement("div", null, 
            this.weekdays()
          )
        ), 
        React.createElement("div", {className: "datepicker__month"}, 
          this.weeks()
        )
      )
    );
  }
});

module.exports = Calendar;
