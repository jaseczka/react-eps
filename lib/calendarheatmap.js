/** @jsx React.DOM */
var React = require('react');
var d3 = require('d3');
var Chart = require('./chart').Chart;
var _ = require('lodash');
var moment = require('moment');

var Day = React.createClass({displayName: "Day",

    propTypes: {
        handleClick: React.PropTypes.func,
        date: React.PropTypes.instanceOf(Date).isRequired,
        textContent: React.PropTypes.string,
        x: React.PropTypes.number.isRequired,
        y: React.PropTypes.number.isRequired,
        fill: React.PropTypes.string,
        stroke: React.PropTypes.string,
        width: React.PropTypes.number.isRequired,
        height: React.PropTypes.number.isRequired,
        value: React.PropTypes.number,
        counterEnabled: React.PropTypes.bool
    },

    getDefaultProps: function() {
        return {
            handleClick: null,
            textContent: "",
            fill: "none",
            value: 0,
            stroke: "gray",
            counterEnabled: false
        }
    },

    renderCounter: function() {
        if (!this.props.counterEnabled || this.props.value === 0)
            return;
        var node = this.refs.dayValueText.getDOMNode();
        try { //getBBox throws when node is not rendered
            var box = node.getBBox();
        } catch (e) {
            var box = {x: 0, y:0, width: 0, height: 0};
        }
        var container = this.refs.dayValueContainer.getDOMNode();
        d3.select(container)
            .append("rect")
            .attr("x", box.x)
            .attr("y", box.y)
            .attr("width", box.width)
            .attr("height", box.height)
            .attr("fill", "gray")
            .attr("rx", "5")
            .attr("ry", "5");

        var text = d3.select(node);

        d3.select(container)
            .append("text")
            .attr("x", text.attr("x"))
            .attr("y", text.attr("y"))
            .attr("text-anchor", "end")
            .attr("fill", "white")
            .text(this.props.value);
    },

    componentDidMount: function() {
        this.renderCounter();
    },

    componentDidUpdate: function() {
        this.renderCounter();
    },

    render: function() {
        var counter = null;
        if (this.props.counterEnabled && this.props.value !== 0) {
            counter =
                React.createElement("g", {
                    className: "chm-day-counter", 
                    ref: "dayValueContainer"
                }, 
                    React.createElement("text", {
                        ref: "dayValueText", 
                        className: "chm-day-counter-value", 
                        textAnchor: "end", 
                        x: this.props.x + this.props.width - 10, 
                        y: this.props.y + 20, 
                        fill: "white"
                    }, 
                    this.props.value
                    )
                );
        }

        return (
            React.createElement("g", {
                className: "chm-day", 
                onClick: this.props.handleClick
            }, 
                React.createElement("rect", {
                    className: "chm-day-rect", 
                    fill: this.props.fill, 
                    width: this.props.width, 
                    height: this.props.height, 
                    x: this.props.x, 
                    y: this.props.y, 
                    strokeWidth: "1", 
                    stroke: this.props.stroke}
                ), 
                counter, 
                React.createElement("text", {
                    className: "chm-day-text", 
                    x: this.props.x + 10, 
                    y: this.props.y + this.props.height - 10
                }, 
                this.props.textContent
                )
            )
        )
    }
});

var XAxis = React.createClass({displayName: "XAxis",

    propTypes: {
        width: React.PropTypes.number,
        margins: React.PropTypes.shape({
            top: React.PropTypes.number,
            right: React.PropTypes.number,
            bottom: React.PropTypes.number,
            left: React.PropTypes.number
        })
    },

    render: function() {
        var dayWidth = this.props.width / 7;
        var weekdays = [0, 1, 2, 3, 4, 5, 6];
        var firstDay = moment().startOf('week');

        weekdays = weekdays.map(function(day) {
            var weekday = moment(firstDay).add(day, 'day');
            return weekday.format('ddd');
        });

        weekdays = weekdays.map(function(day, i) {
            var x = i * dayWidth + 5;
            var y = this.props.margins.top - 10;
            return (
                React.createElement("text", {
                    className: "weekday-title", 
                    x: x, 
                    y: y, 
                    key: day, 
                    fill: "black"
                }, 
                day
                )
            );
        }, this);

        return (
            React.createElement("g", {className: "xaxis axis"}, weekdays)
        )
    }
});

var YAxis = React.createClass({displayName: "YAxis",

    propTypes: {
        width: React.PropTypes.number,
        height: React.PropTypes.number,
        margins: React.PropTypes.shape({
            top: React.PropTypes.number,
            right: React.PropTypes.number,
            bottom: React.PropTypes.number,
            left: React.PropTypes.number
        }),
        dateFrom: React.PropTypes.instanceOf(Date),
        dateTo: React.PropTypes.instanceOf(Date),
        dayHeight: React.PropTypes.number
    },

    renderYAxis: function() {
        var dateFrom = moment(this.props.dateFrom);
        var dateTo = moment(this.props.dateTo);
        var labels = [];

        var i = 0;
        for (var day = dateFrom;
             day <= dateTo;
             day.add(1, 'week')) {
            var weekAgo = moment(day).subtract(1, 'week');
            if (!(weekAgo.isSame(day, 'month'))) {
                labels.push({
                    row: i,
                    name: day.format('MMMM YYYY')
                });
            }
            ++i;
        }

        labels = labels.map(function(obj) {
            var yPos = obj.row * this.props.dayHeight;
            var trans = "translate(0," + yPos + ")";
            return (
                React.createElement("g", {transform: trans, key: 'yaxis'+obj.name}, 
                    React.createElement("text", {
                        x: "0", 
                        y: "0", 
                        transform: "rotate(90)"
                    }, 
                    obj.name
                    )
                )
            );
        }, this);

        return labels;
    },

    render: function() {
        var xPos = this.props.width + 10;
        var trans = "translate(" + xPos + ", " + this.props.margins.top + ")";
        return (
            React.createElement("g", {
                className: "yaxis axis", 
                transform: trans
            }, 
            this.renderYAxis()
            )
        )
    }
});

var DataSeries = React.createClass({displayName: "DataSeries",

    propTypes: {
        handleClick: React.PropTypes.func,
        data: React.PropTypes.arrayOf(React.PropTypes.shape({
            date: React.PropTypes.instanceOf(Date),
            value: React.PropTypes.number
        })),
        dayWidth: React.PropTypes.number,
        dayHeight: React.PropTypes.number,
        margins: React.PropTypes.shape({
            top: React.PropTypes.number,
            right: React.PropTypes.number,
            bottom: React.PropTypes.number,
            left: React.PropTypes.number
        }),
        dateFrom: React.PropTypes.instanceOf(Date),
        dateTo: React.PropTypes.instanceOf(Date),
        fillMin: React.PropTypes.string,
        fillMax: React.PropTypes.string
    },

    render: function() {
        var timeScale = d3.time.scale()
            .domain([this.props.dateFrom, this.props.dateTo])
            .ticks(d3.time.day);
        var dateFrom = this.props.dateFrom;
        var dateTo = this.props.dateTo;
        var dayWidth = this.props.dayWidth;
        var dayHeight = this.props.dayHeight;

        //day of week of first date in range
        var weekdayFormat = d3.time.format("%w");
        var dateFromWeekday = parseInt(weekdayFormat(dateFrom));

        var maxValue = 0;
        for (var i = 0; i < this.props.data.length; ++i) {
            var value = this.props.data[i].value;
            var date = this.props.data[i].date;
            if (date >= dateFrom && date <= dateTo && value > maxValue) {
                maxValue = value;
            }
        }

        var colorScale = d3.scale.linear()
            .domain([0, maxValue])
            .range([this.props.fillMin, this.props.fillMax]);


        var days = timeScale.map(function(day, i) {
            var dayOfWeek = (i + dateFromWeekday) % 7;
            var month = day.getMonth();
            var year = day.getFullYear();
            var x = (dayOfWeek) * dayWidth;
            var y = (i +  dateFromWeekday - dayOfWeek) / 7  * dayHeight;
            var data = _.find(this.props.data, {date: day});
            var value = (data !== undefined) ? data.value : 0;
            var handleThisClick = this.props.handleClick.bind(null, day);

            return (
                React.createElement(Day, {
                    handleClick: handleThisClick, 
                    date: day, 
                    value: value, 
                    textContent: d3.time.format("%_d")(day), 
                    x: x, 
                    y: y, 
                    width: dayWidth, 
                    height: dayHeight, 
                    fill: colorScale(value), 
                    counterEnabled: this.props.counterEnabled, 
                    key: day}
                )
            );
        }, this);

        var firstOfMonth = _.filter(timeScale, function(date) {
            return date.getDate() == 1;
        });

        var monthDividers = firstOfMonth.map(function(date) {
            var weekday = parseInt(d3.time.format("%w")(date));
            var daysNo = (date - dateFrom) / 1000 / 60 / 60 / 24 + 1;
            var week = Math.ceil((daysNo + dateFromWeekday) / 7);
            var d = "";
            if (weekday === 0) {
                var startX = 0;
                var startY = (week - 1) * dayHeight;
                var h = 7 * dayWidth;
                d = "M" + startX + " " + startY + " h " + h;
            } else {
                var startX = 0;
                var startY = week * dayHeight;
                var h1 = weekday * dayWidth;
                var h2 = (7 - weekday) * dayWidth;
                var v = -dayHeight;
                d = "M" + startX + " " + startY + " h " + h1 + " v " + v + " h " + h2;
            }


            return (
                React.createElement("path", {
                    className: "chm-month-divider", 
                    d: d, 
                    fill: "none", 
                    stroke: "black", 
                    strokeWidth: "2", 
                    key: date}
                )
            );
        });


        var trans = "translate(0," + this.props.margins.top + ")";

        return (
            React.createElement("g", {
                transform: trans
            }, 
                days, 
                monthDividers
            )
        );
    }
});

var CalendarHeatMap = React.createClass({displayName: "CalendarHeatMap",

    propTypes: {
        data: React.PropTypes.arrayOf(React.PropTypes.shape({
            date: React.PropTypes.instanceOf(Date),
            value: React.PropTypes.number
        })),
        dateFrom: React.PropTypes.instanceOf(Date),
        dateTo: React.PropTypes.instanceOf(Date),
        width: React.PropTypes.number,
        height: React.PropTypes.number,
        dayHeight: React.PropTypes.number,
        dayWidth: React.PropTypes.number,
        margins: React.PropTypes.shape({
            top: React.PropTypes.number,
            right: React.PropTypes.number,
            bottom: React.PropTypes.number,
            left: React.PropTypes.number
        }),
        title: React.PropTypes.string,
        counterEnabled: React.PropTypes.bool,
        fillMin: React.PropTypes.string,
        fillMax: React.PropTypes.string,
        handleClick: React.PropTypes.func
    },

    getDefaultProps: function() {
        var dateFrom = new Date();
        var dateTo = new Date();
        dateTo.setDate(dateTo.getDate() + 27);

        return {
            data: [],
            margins: {top: 30, right: 30, bottom: 10, left: 10},
            title: 'Calendar Heat Map',
            dateFrom: dateFrom,
            dateTo: dateTo,
            counterEnabled: false,
            fillMin: "white",
            fillMax: "magenta",
            handleClick: null
        };
    },

    render: function() {
        var margins = this.props.margins;
        var sideMargins = margins.left + margins.right;
        var topBottomMargins = margins.top + margins.bottom;

        var dateFrom = this.props.dateFrom;
        dateFrom = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate());
        var dateTo = d3.time.day(this.props.dateTo);

        var dayWidth = this.props.dayWidth;
        var width = this.props.width;
        var dayHeight = this.props.dayHeight;
        var height = this.props.height;

        if (!width) {
            if (!dayWidth) {
                dayWidth = 70;
            }
            width = 7 * dayWidth + sideMargins;
        } else {
            dayWidth = (width - sideMargins) / 7;
        }

        //day of week of first date in range
        var weekdayFormat = d3.time.format("%w");
        var dateFromWeekday = parseInt(weekdayFormat(dateFrom));
        var daysNo = (dateTo - dateFrom) / 1000 / 60 / 60 / 24 + 1;
        var rows = Math.ceil((daysNo + dateFromWeekday) / 7);

        if (!height) {
            if (!dayHeight) {
                dayHeight = 50;
            }
            height = dayHeight * rows + topBottomMargins;
        } else {
            dayHeight = (height - topBottomMargins) / rows;
        }

        var trans = "translate(" + margins.left + ",0)";

        return (
            React.createElement(Chart, {
                width: width, 
                height: height, 
                title: this.props.title
            }, 
                React.createElement("g", {
                    className: "calendar-heat-map", 
                    transform: trans
                }, 
                    React.createElement(DataSeries, {
                        handleClick: this.props.handleClick, 
                        data: this.props.data, 
                        dayWidth: dayWidth, 
                        dayHeight: dayHeight, 
                        margins: this.props.margins, 
                        dateFrom: dateFrom, 
                        dateTo: dateTo, 
                        counterEnabled: this.props.counterEnabled, 
                        fillMin: this.props.fillMin, 
                        fillMax: this.props.fillMax}
                    ), 
                    React.createElement(XAxis, {
                        width: width - sideMargins, 
                        margins: margins}
                    ), 
                    React.createElement(YAxis, {
                        width: width - sideMargins, 
                        height: height - topBottomMargins, 
                        margins: margins, 
                        dateFrom: dateFrom, 
                        dateTo: dateTo, 
                        dayHeight: dayHeight}
                    )
                )
            )
        );
    }
});

exports.CalendarHeatMap = CalendarHeatMap;
