/** @jsx React.DOM */

/**
 * Created by Magdalena on 2015-01-26.
 */

var React = require('react');
var _ = require('lodash');

var FilterOption = React.createClass({displayName: "FilterOption",

    render: function() {
        return (
            React.createElement("label", {className: "filter-option"}, 
                React.createElement("input", {
                    type: "checkbox", 
                    checked: this.props.isEnabled, 
                    onChange: this.props.toggleOption}
                ), 
                    this.props.name
            )
        );
    }
});

var FilterSection = React.createClass({displayName: "FilterSection",

    render: function() {
        var isOneOff = false;

        var options = this.props.options.map(function(option, index) {
            if (!option.include) {
                isOneOff = true;
            }

            return (
                React.createElement(FilterOption, {
                    toggleOption: this.props.toggleOption.bind(null, index), 
                    key: "opt" + index, 
                    name: option.displayName, 
                    isEnabled: option.include}
                )
            );
        }, this);

        if (this.props.toggleAllEnabled) {
            options.unshift(
                React.createElement(FilterOption, {
                    toggleOption: this.props.toggleAllOption, 
                    key: "optall", 
                    name: "all", 
                    isEnabled: !isOneOff}
                )
            );
        }

        return (
            React.createElement("div", {className: "filter-section"}, 
                React.createElement("dt", {className: "filter-title"}, this.props.name), 
                React.createElement("dd", null, options)
            )
        );
    }
});

var FilterPanel = React.createClass({displayName: "FilterPanel",

    propTypes: {
        filters: React.PropTypes.arrayOf(React.PropTypes.shape({
            displayName: React.PropTypes.string,
            test: React.PropTypes.func,
            options: React.PropTypes.arrayOf(React.PropTypes.shape({
                displayName: React.PropTypes.any,
                include: React.PropTypes.bool
            }))
        })).isRequired,
        toggleAllEnabled: React.PropTypes.bool
    },

    getDefaultProps: function() {
        return {
            toggleAllEnabled: false
        }
    },

    render: function() {
        var filters = this.props.filters.map(function(filter, index) {
            return (
                React.createElement(FilterSection, {
                    toggleOption: this.props.toggleOption.bind(null, index), 
                    toggleAllOption: this.props.toggleAllOption.bind(null, index), 
                    toggleAllEnabled: this.props.toggleAllEnabled, 
                    name: filter.displayName, 
                    options: filter.options, 
                    key: "f" + index}
                )
            )
        }, this);

        return (
            React.createElement("dl", {className: "filter-panel"}, 
                filters
            )
        );
    }
});

var FilterPanelMixin = {
    componentWillMount: function() {
        this.assertFiltersDefined();
    },

    assertFiltersDefined: function() {
        if (!this.state.filters) {
            throw new Error('FilterPanelMixin requires filters object defined in getInitialState.');
        }
    },

    toggleOption: function(filterIndex, optionIndex) {
        var newValue = !this.state.filters[filterIndex].options[optionIndex].include;
        var optionsMutation = {};
        optionsMutation[optionIndex] = {include: {$set: newValue}};
        var mutation = {};
        mutation[filterIndex] = {options: optionsMutation};
        var newFilters = React.addons.update(this.state.filters, mutation);

        this.setState({
            filters: newFilters
        });
    },

    toggleAllOption: function(filterIndex, event) {
        var include = event.target.checked;
        var optionsMutation = {};
        var mutation = {};
        for (var i = 0; i < this.state.filters[filterIndex].options.length; ++i) {
            if (this.state.filters[filterIndex].options[i].include !== include) {
                optionsMutation[i] = {include: {$set: include}};
            }
        }
        mutation[filterIndex] = {options: optionsMutation};
        var newFilters = React.addons.update(this.state.filters, mutation);

        this.setState({
            filters: newFilters
        });
    },

    getFilteredData: function(rawData) {
        var filters = this.state.filters;
        var filteredData = [];
        if (filters != null) {
            filteredData = _.filter(rawData, function(obj) {
                //all filter tests must return true
                for (var filter = 0; filter < filters.length; ++filter) {
                    if (!filters[filter].test(obj)) {
                        return false;
                    }
                }
                return true;
            });
        }
        return filteredData;
    },

    renderFilterPanel: function(args) {
        args = args || {};
        return (
            React.createElement(FilterPanel, {
                filters: this.state.filters, 
                toggleOption: this.toggleOption, 
                toggleAllOption: this.toggleAllOption, 
                toggleAllEnabled: args.toggleAllEnabled}
            )
        );
    }
};

exports.FilterPanel = FilterPanel;
exports.FilterPanelMixin = FilterPanelMixin;