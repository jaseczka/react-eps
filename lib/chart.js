/** @jsx React.DOM */
var React = require('react');
var d3 = require('d3');

exports.Chart = React.createClass({displayName: "Chart",
	render: function() {
		return (
			React.createElement("div", {className: "chart"}, 
				React.createElement("h3", null, this.props.title), 
				React.createElement("svg", {width: this.props.width, height: this.props.height}, this.props.children)
			)
		);
	}
});