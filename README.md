# README #

This is a set of ReactJS components developed for Eastside Preparatory School. 

## Demo ##

This project is used as node module dependency in [assignments-calendar](bitbucket.org/jaseczka/assignments-calendar) project. Exemplary site that lives in example/ directory is deprecated and may no longer work (it's an early version of assignments-calendar).

## Components ##

Documentation should be available in project [wiki](https://bitbucket.org/jaseczka/react-eps/wiki/Home).

### CalendarHeatMap ###

Component build with help of d3 library. The aim was to create highly customizable and ready-to-use calendar heatmap of e.g. upcoming deadlines or events. Calendar also features onClick event that is binded to Day subcomponents.

### FilterPanel ###

Simple graphical representation of filters in the form of definition list. There is also available FilterPanelMixin that provides basic logic for toggling filters and filtering data.

## How do I get set up? ##

#### For node usage ####

* Without cloning the repository:
```
#!bash
    
$ npm install git+https://jaseczka@bitbucket.org/jaseczka/react-eps.git
```
* With cloned repository:
```
#!bash

$ npm install /path/to/repo
```



#### For browser usage ####

Simply copy react-eps.js from dist/ directory.

In order to create require-able bundle that may be used in browser run:

```
#!bash

$ browserify -x react -x react/addons -r react-eps > yourbundle.js
```